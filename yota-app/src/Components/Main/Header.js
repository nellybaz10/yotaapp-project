import React, { Component } from 'react';
import logo from '../../Images/yota_logo.png'
import whatsapp from '../../Images/whatsapp_icon.png';
import {Link} from 'react-router-dom';
import '../../Styles/Header.css';


class Header extends Component {

  render() {
    const width = 150;

    return (
      <div className="Header">
          <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <Link to="/" className="navbar-brand" href="/ETN-LIBERIA_Project/"><img className="logo" src={logo} style={{width:width+'px'}} /> </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
              </ul>
              <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link" href="tel:+250 784 650 455"> <img className="whatsapp_number" src={whatsapp} /> +250 784 650 455 </a>
              </li>
                <li className="nav-item">
                  <Link to="/login" className="nav-link login" >Login </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link sign-up">Sign up</a>
                </li>

                <li className="nav-item">
                  <Link to="/report" className="nav-link " href="get-involved.php">Report Issue</Link>
                </li>
              </ul>



            </div>
          </nav>
      </div>
    );
  }
}

export default Header;
