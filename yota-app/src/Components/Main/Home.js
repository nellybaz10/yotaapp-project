import React, { Component } from 'react';
import '../../Styles/Home.css';
import Most_Booked from '../Sub/Most_Booked';
import Featured from '../Sub/Featured';
import Right_Sidebar from '../Sub/Right_Sidebar';

class Home extends Component {
  render() {
    return (
      <div className="Home">
          <div className="jumbotron jumbotron-fluid">
            <div className="container">
              <h1 className="display-4">Find and book services in Kigali</h1>
              <p className="lead">Search through over 1000 registered services.</p>

                <form className="form-inline search-div-home">
                <input id="search-input-box" className="form-control" placeholder="Search service men all over Kigali" />
                <input className="btn btn-outline-success" type="button" value="Search" />
                </form>
            </div>
          </div>

          <div className="row">
            <div className="column_big">
              <Most_Booked />
              <Featured />
            </div>
            <div className="column_small">
              <Right_Sidebar />
            </div>
          </div>


      </div>
    );
  }
}

export default Home;
