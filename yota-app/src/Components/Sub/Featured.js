import React, { Component } from 'react';
import carpenter from '../../Images/carpenter.jpg';
import plumber from '../../Images/plumber.jpg';
import electrician from '../../Images/electrician.jpg';
import photographer from '../../Images/photographer.jpg';
import mechanic from '../../Images/mechanic.jpg';
import face from '../../Images/face.jpg';
import rating from '../../Images/rating.png';
import  '../../Styles/featured.css';

class Featured extends Component {


  render() {
    const data = {
      face:{
        image: face,
        title: "John Mugabiri",
        location: "Rugando, Kigali"
      },




    }

    return (
      <div className="Featured">
        <h3 className=" container first-title">Most Hired Service men</h3>
        <div className="container">
          <div className="row row1">

              <div className="w3-card-4">
                <img src={data.face.image} alt="" />
                <div className="w3-container w3-center">
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} /><p>{data.face.title}</p>
                  <p className="location">Location: {data.face.location}</p>
                  <p className="skill">Skill: Carpenter</p>
                </div>
              </div>

              <div className="w3-card-4">
                <img src={data.face.image} alt="" />
                <div className="w3-container w3-center">
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} /><p>{data.face.title}</p>
                  <p className="location">Location: {data.face.location}</p>
                  <p className="skill">Skill: Carpenter</p>
                </div>
              </div>

              <div className="w3-card-4">
                <img src={data.face.image} alt="" />
                <div className="w3-container w3-center">
                  <img className="rating" src={rating} /><p>{data.face.title}</p>
                  <p className="location">Location: {data.face.location}</p>
                  <p className="skill">Skill: Carpenter</p>
                </div>
              </div>

              <div className="w3-card-4">
                <img src={data.face.image} alt="" />
                <div className="w3-container w3-center">
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} />
                  <img className="rating" src={rating} /><p>{data.face.title}</p>
                  <p className="location">Location: {data.face.location}</p>
                  <p className="skill">Skill: Carpenter</p>
                </div>
              </div>

            </div>
        </div>
      </div>
    );
  }
}

export default Featured;
