import React, { Component } from 'react';
import '../../Styles/right_sidebar.css';

class Right_Sidebar extends Component {
  render() {
    const width = 60;
    return (
      <div className="Right_Sidebar">
        <h5>All Services</h5>
        <div className="card" style={{width: width+"%"}}>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Carpenter </li>
            <li className="list-group-item">Electrician</li>
            <li className="list-group-item">Plumber</li>
            <li className="list-group-item">Mechanic</li>
            <li className="list-group-item">Photographer</li>
            <li className="list-group-item">Service Six</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Right_Sidebar;
