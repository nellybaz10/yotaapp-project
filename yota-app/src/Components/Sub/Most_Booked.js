import React, { Component } from 'react';
import carpenter from '../../Images/carpenter.jpg';
import plumber from '../../Images/plumber.jpg';
import electrician from '../../Images/electrician.jpg';
import photographer from '../../Images/photographer.jpg';
import mechanic from '../../Images/mechanic.jpg';
import mowering from '../../Images/mowering.jpg';
import fumigation from '../../Images/fumigation.jpg';
import services from '../../Styles/most_booked.css';

class Most_Booked extends Component {


  render() {
    const data = {
      carpenter:{
        image: carpenter,
        title: "Carpenter"
      },

      plumber:{
        image:plumber,
        title: "Plumber"
      },

      electrician:{
        image:electrician,
        title: "Electrician"
      },

      mechanic:{
        image:mechanic,
        title: "Mechanic"
      },

      photographer:{
        image:photographer,
        title: "Photographer"
      },

      mowering:{
        image:mowering,
        title: "Mowering"
      },

      fumigation:{
        image:fumigation,
        title: "Fumigation"
      }

    }

    return (
      <div className="Most_Booked">
        <h3 className=" container first-title">Most Hired Services</h3>
        <div className="container">
          <div className="row row1">
              <div className="w3-card-4">
                <img src={data.plumber.image} alt="" />
                <div className="w3-container w3-center">
                  <p>{data.plumber.title}</p>
                </div>
              </div>

              <div className="w3-card-4">
                <img src={data.electrician.image} alt="" />
                <div className="w3-container w3-center">
                  <p>{data.electrician.title}</p>
                </div>
              </div>


              <div className="w3-card-4">
                <img src={data.mechanic.image} alt="" />
                <div className="w3-container w3-center">
                  <p>{data.mechanic.title}</p>
                </div>
              </div>

              <div className="w3-card-4">
                <img src={data.photographer.image} alt="" />
                <div className="w3-container w3-center">
                  <p>{data.photographer.title}</p>
                </div>
              </div>

              <div className="w3-card-4 extended">
                <img src={data.carpenter.image} alt="" />
                <div className="w3-container w3-center">
                  <p>{data.carpenter.title}</p>
                </div>
              </div>


          </div>
            <div className=" second-row">
              <div className="row row1">
                    <div className="w3-card-4">
                      <img src={data.mowering.image} alt="" />
                      <div className="w3-container w3-center">
                        <p>{data.mowering.title}</p>
                      </div>
                    </div>


                    <div className="w3-card-4">
                      <img src={data.fumigation.image} alt="" />
                      <div className="w3-container w3-center">
                        <p>{data.fumigation.title}</p>
                      </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
    );
  }
}

export default Most_Booked;
