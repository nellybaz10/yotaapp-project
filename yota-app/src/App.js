import React, { Component } from 'react';
import Header from './Components/Main/Header';
import Home from './Components/Main/Home';
import Login from './Components/Main/Login';
import Report from './Components/Main/Report';
import { Route,BrowserRouter as Router } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
          <Header />
              <Route exact path={"/"} component={Home} />
              <Route exact path={"/login"} component={Login} />
              <Route exact path={"/report"} component={Report} />
      </div>


      </Router>
    );
  }
}

export default App;
